package model

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

type ToDo struct {
	gorm.Model
	Title     string `gorm:"unique" json:"title"`
	Completed bool   `json:"completed"`
}

func (p *ToDo) Complete() {
	p.Completed = true
}

func (p *ToDo) Uncomplete() {
	p.Completed = false
}

// DBMigrate will create and migrate the tables, and then make the some relationships if necessary
func DBMigrate(db *gorm.DB) *gorm.DB {
	db.AutoMigrate(&ToDo{})
	return db
}
