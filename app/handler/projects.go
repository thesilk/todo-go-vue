package handler

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	"gitlab.com/thesilk/todo-api/app/model"
)

func GetAllToDos(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	toDo := []model.ToDo{}
	db.Find(&toDo)
	respondJSON(w, http.StatusOK, toDo)
}

func CreateToDo(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	toDo := model.ToDo{}

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&toDo); err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	if err := db.Save(&toDo).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusCreated, toDo)
}

func GetToDo(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	id := vars["id"]
	toDo := getToDoOr404(db, id, w, r)
	if toDo == nil {
		return
	}
	respondJSON(w, http.StatusOK, toDo)
}

func UpdateToDo(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	id := vars["id"]
	toDo := getToDoOr404(db, id, w, r)
	if toDo == nil {
		return
	}

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&toDo); err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	if err := db.Save(&toDo).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusOK, toDo)
}

func DeleteToDo(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	id := vars["id"]
	toDo := getToDoOr404(db, id, w, r)
	if toDo == nil {
		return
	}
	if err := db.Delete(&toDo).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusNoContent, nil)
}

func CompleteToDo(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	id := vars["id"]
	toDo := getToDoOr404(db, id, w, r)
	if toDo == nil {
		return
	}
	toDo.Complete()
	if err := db.Save(&toDo).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusOK, toDo)
}

func UncompleteToDo(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	id := vars["id"]
	toDo := getToDoOr404(db, id, w, r)
	if toDo == nil {
		return
	}
	toDo.Uncomplete()
	if err := db.Save(&toDo).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusOK, toDo)
}

// getToDoOr404 gets a project instance if exists, or respond the 404 error otherwise
func getToDoOr404(db *gorm.DB, id string, w http.ResponseWriter, r *http.Request) *model.ToDo {
	toDo := model.ToDo{}
	u, _ := strconv.ParseUint(id, 10, 64)
	if err := db.First(&toDo, u).Error; err != nil {
		respondError(w, http.StatusNotFound, err.Error())
		return nil
	}
	return &toDo
}
