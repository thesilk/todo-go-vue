package main

import (
	"gitlab.com/thesilk/todo-api/app"
)

func main() {

	app := &app.App{}
	app.Initialize()
	app.Run(":3000")
}
