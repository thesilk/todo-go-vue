import { shallowMount, Wrapper, createLocalVue } from '@vue/test-utils';
import BootstrapVue from 'bootstrap-vue';

import TodoItem from '@/components/home/TodoItem.vue';
import { Todo as TodoType } from '@/components/todo.interface';

describe('TodoItem.vue', () => {
  let localVue: any;
  let wrapper: Wrapper<any>;
  let todo: TodoType[];
  beforeAll(() => {
    localVue = createLocalVue();
    localVue.use(BootstrapVue);
    todo = [
      {
        ID: 23,
        CreatedAt: '0',
        UpdatedAt: '0',
        DeletedAt: '0',
        title: 'do something',
        completed: false,
      },
    ];
    wrapper = shallowMount(TodoItem, {
      localVue,
      propsData: { todo },
    });
  });
  it('should return complete button text', () => {
    expect(wrapper.vm.getCompleteText(todo[0])).toMatch('Complete');
    todo[0].completed = true;
    expect(wrapper.vm.getCompleteText(todo[0])).toMatch('Uncomplete');
  });
});
