export interface Project {
  ID: number;
  CreatedAt: string;
  UpdatedAt: string;
  DeletedAt: string;
  title: string;
  archived: boolean;
  task?: Task[];
}

export interface Task {
  ID: number;
  CreatedAt: string;
  UpdatedAt: string;
  DeletedAt: string;
  title: string;
  done: boolean;
  projectID: number;
}
