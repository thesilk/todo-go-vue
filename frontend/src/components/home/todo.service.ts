import Axios from 'axios-observable';
import { Observable } from 'rxjs';
import { retry } from 'rxjs/operators';
import { Todo as TodoType } from '@/components/todo.interface';
import { AxiosResponse } from 'axios';

export default class TodoService {
  getTodos(): Observable<AxiosResponse<TodoType>> {
    return Axios.get('http://localhost:3000/todos').pipe(retry(3));
  }
}
