export interface Todo {
  ID: number;
  CreatedAt: string;
  UpdatedAt: string;
  DeletedAt: string;
  title: string;
  completed: boolean;
}
