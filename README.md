# Go Todo REST API + Vue Example

This repo contains a small REST API to handle ToDos written in golang. It uses a sqlite3 database with GORM and gorilla/mux.
Also a small vue frontend is available with bootstrap-vue. The focus in this repository is to learn the concepts of vue so
the styling with CSS had no priority.

### Run REST API

```bash
$ go build -o todo-api main.go
$ ./todo-api
```

### Run Vue Frontend

```bash
$ npm install -g @vue/cli
$ cd frontend
$ npm install
$ npm run start
```

![Vue ToDo App](screenshot/todo_vue.gif)
